<?php

/**
 * This is the model class for table "tbl_info".
 *
 * The followings are the available columns in table 'tbl_info':
 * @property integer $id
 * @property string $category
 * @property string $title
 * @property string $content
 * @property string $image_path
 * @property integer $is_active
 */
class Info extends CActiveRecord
{
	public $upload_dir = 'images/info/';
	public $image_file;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_info';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('is_active', 'numerical', 'integerOnly'=>true),
			array('category', 'length', 'max'=>100),
			array('title', 'length', 'max'=>150),
			array('image_path', 'length', 'max'=>250),
			array('content', 'safe'),
			array('upload_dir', 'directory_available'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, category, title, content, image_path, is_active', 'safe', 'on'=>'search'),
		);
	}

	public function directory_available($attribute, $params)
	{
		$savePath = sprintf('%s..%s'
			, Yii::app()->getBasePath() . DIRECTORY_SEPARATOR
			, DIRECTORY_SEPARATOR . str_replace('/', DIRECTORY_SEPARATOR, $this->upload_dir)
		);
		if (! file_exists($savePath)) {
			if (is_writable(dirname($savePath))) {
				if (! mkdir($savePath, 0777, true)) {
					$this->addError('image_file', 'Cannot create directory: '. $savePath);	
				}
			} else {
				$this->addError('image_file', 'Cannot create directory: '. dirname($savePath));
			}
		} elseif (! is_writable($savePath)) {
			$this->addError('image_file', 'Permission denied on: '. $savePath);
		}
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'category' => 'Kategori',
			'title' => 'Judul',
			'content' => 'Isi',
			'image_path' => 'Gambar',
			'is_active' => 'Aktif',
			'image_file' => 'Gambar',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('category',$this->category,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('content',$this->content,true);
		$criteria->compare('is_active',$this->is_active,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Info the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	public function beforeSave()
	{
		$this->image_file = CUploadedFile::getInstance($this, 'image_file');
		if ($this->image_file != null) {
			$basedir = $this->upload_dir;
			$filename = sprintf('%s.%s'
				, sha1($this->image_file->getName() . time())
				, $this->image_file->getExtensionName()
			);
			$this->image_file->saveAs(Yii::app()->getBasePath() . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . $basedir . $filename);
			$this->image_path = $basedir . $filename;
		}
		return parent::beforeSave();
	}	
	public function scopes()
	{
		return array(
			'categoryList'=>array(
                'condition'=>'is_active = 1',
                'group'=>'category'
            ),
		);
	}

	public function randomize($category = null){
		$categories = CJSON::decode($category);
		
		$criteria = new CDbCriteria;
		$criteria->limit = 1;
		$criteria->addCondition('is_active = 1');
		$criteria->order = 'RAND()';

		if(count($categories) >= 1){
			$categories = CJSON::decode($category);
			$criteria->addInCondition('category', $categories);
		}

		return $this->find($criteria);
	}
}
