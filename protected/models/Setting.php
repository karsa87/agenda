<?php

/**
 * This is the model class for table "tbl_setting".
 *
 * The followings are the available columns in table 'tbl_setting':
 * @property integer $id
 * @property string $date_from
 * @property string $date_to
 * @property string $text_footer
 * @property integer $limit_row
 * @property string $limit_column
 * @property integer $interval_time
 * @property string facebook
 * @property string url_facebook
 * @property string twitter
 * @property string url_twitter
 * @property string email1
 * @property string email2
 * @property string website
 * @property string text_header1
 * @property string text_header2
 * @property string $updated
 * @property integer $updated_by
 */
class Setting extends CActiveRecord
{
	public $column_list = array();
	public $sidebar_list = array();
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_setting';
	}

	public function init(){
		$this->sidebar = null;
		$this->interval_time = 5;
		$this->date_from = null;
		$this->date_to = null;
		$this->text_footer = "Agenda Elektronik DKP Kota Malang";
		$this->column_list = array('date_schedule' => 'Hari / Tanggal Acara', 'time_schedule'=>'Waktu', 'place'=>'Tempat', 'name'=>'Acara','disposisi_nama'=>'Disposisi');
		$this->limit_row = 10;
		$this->limit_column = CJSON::encode($this->column_list);
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('column_list', 'required'),
			array('sidebar', 'length', 'max'=>256),
			array('limit_row, interval_time, updated_by', 'numerical', 'integerOnly'=>true),
			array('date_from', 'compare', 'operator'=>'<=', 'compareAttribute'=>'date_to'),
			array('text_footer', 'length', 'max'=>250),
			array('email2', 'length', 'max'=>225),
			array('email2', 'email'),
			array('email1', 'length', 'max'=>225),
			array('email1', 'email'),
			array('facebook', 'length', 'max'=>225),
			array('url_facebook', 'length', 'max'=>225),
			array('twitter', 'length', 'max'=>225),
			array('url_twitter', 'length', 'max'=>225),
			array('website', 'length', 'max'=>225),
			array('text_header1', 'length', 'max'=>225),
			array('text_header2', 'length', 'max'=>225),
			array('sidebar_list, date_from, date_to', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, sidebar, date_from, date_to, text_footer, limit_row, limit_column, interval_time, facebook, url_facebook, twitter, url_twitter, email1, email2, website, text_header1, text_header2, updated, updated_by', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'sidebar' => 'Sidebar',
			'date_from' => 'Date From',
			'date_to' => 'Date To',
			'text_footer' => 'Text Footer',
			'limit_row' => 'Max Row',
			'limit_column' => 'Display Column',
			'interval_time' => 'Interval Time in Minutes',
			'updated' => 'Updated',
			'updated_by' => 'Updated By',
			'column_list' => 'Column List',
			'facebook' => 'Facebook',
			'url_facebook' => 'Url Facebook',
			'twitter' => 'Twitter',
			'url_twitter' => 'Url Twitter',
			'email1' => 'Email 1',
			'email2' => 'Email 2',
			'website' => 'Website',
			'text_header1' => 'Text Header 1',
			'text_header2' => 'Text Header 2',
		);
	}
	public function dateToList()
	{
		return array(
			'0' =>'Hari ini',
			'1'	=>'Besok',
			'2'	=>'Lusa',
			'7'	=>'Minggu depan',
	 		'30'=>'Bulan depan',
		);
	}
	public function dateFromList()
	{
		return array(
			'0' =>'Hari ini',
			'-1'	=>'Kemarin',
			'-2'	=>'Kemarin Lusa',
			'-7'	=>'Minggu Kemairn',
	 		'-30'=>'Bulan Kemarin',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('sidebar',$this->sidebar,true);
		$criteria->compare('date_from',$this->date_from,true);
		$criteria->compare('date_to',$this->date_to,true);
		$criteria->compare('text_footer',$this->text_footer,true);
		$criteria->compare('limit_row',$this->limit_row);
		$criteria->compare('limit_column',$this->limit_column,true);
		$criteria->compare('interval_time',$this->interval_time);
		$criteria->compare('facebook',$this->facebook);
		$criteria->compare('url_facebook',$this->url_facebook);
		$criteria->compare('twitter',$this->twitter);
		$criteria->compare('url_twitter',$this->url_twitter);
		$criteria->compare('email1',$this->email1);
		$criteria->compare('email2',$this->email2);
		$criteria->compare('website',$this->website);
		$criteria->compare('text_header1',$this->text_header1);
		$criteria->compare('text_header2',$this->text_header2);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Setting the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public function beforeSave()
	{
		$this->updated_by = Yii::app()->user->id;
		return parent::beforeSave();
	}

	public static function getDefault(){
		$default = self::model()->find();

		if(empty($default)){
			$default = new Setting;
			$default->save();
		}

		return $default;
	}

	public function beforeValidate()
	{
		$this->limit_column = CJSON::encode($this->column_list);
		$this->sidebar = CJSON::encode($this->sidebar_list);

		return parent::beforeValidate();
	}
	public function afterFind()
	{
		$this->column_list = CJSON::decode($this->limit_column);
		$this->sidebar_list = CJSON::decode($this->sidebar);

		return parent::afterFind();
	}
}
