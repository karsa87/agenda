<?php

/**
 * This is the model class for table "tbl_event".
 *
 * The followings are the available columns in table 'tbl_event':
 * @property integer $id
 * @property string $name
 * @property string $schedule
 * @property string $place
 * @property string $remark
 * @property integer $create_by
 * @property string $create_at
 * @property integer $updated_by
 * @property string $updated_at
 */
class Event extends CActiveRecord
{
	public $date_schedule;
	public $time_schedule;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_event';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('is_active', 'numerical', 'integerOnly'=>true),
			array('name, schedule, place', 'required'),
			array('name, remark', 'length', 'max'=>250),
			array('surat', 'length', 'max'=>128),
			array('place', 'length', 'max'=>150),
			array('schedule, disposisi_nama, disposisi_jabatan, tgl_surat', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, schedule, place, remark, surat, disposisi_nama, disposisi_jabatan, tgl_surat, is_active', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Acara',
			'schedule' => 'Tanggal',
			'date_schedule' => 'Tanggal Acara',
			'time_schedule' => 'Jam Acara',
			'place' => 'Tempat',
			'surat' => 'No. Surat',
			'tgl_surat' => 'Tgl. Surat',
			'disposisi_nama' => 'Disposisi Nama',
			'disposisi_jabatan' => 'Disposisi Jabatan',
			'remark' => 'Keterangan',
			'create_by' => 'Dibuat oleh',
			'create_at' => 'Dibuat pada',
			'updated_by' => 'Diubah oleh',
			'updated_at' => 'Diubah pada',
			'is_active' => 'Aktif'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('name',$this->name,true);
		$criteria->compare('schedule',$this->schedule,true);
		$criteria->compare('surat',$this->surat,true);
		$criteria->compare('place',$this->place,true);
		$criteria->compare('disposisi_nama',$this->disposisi_nama,true);
		$criteria->compare('disposisi_jabatan',$this->disposisi_jabatan,true);
		$criteria->compare('is_active',$this->is_active,true);

		$criteria->order = 'schedule DESC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Event the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function beforeSave()
	{
		$this->updated_by = Yii::app()->user->id;
		$this->updated_at = new CDbExpression('NOW()');

		if ($this->isNewRecord) {
			$this->create_by = $this->updated_by;
			$this->create_at = $this->updated_at;
		}
		return parent::beforeSave();
	}
	public function afterFind()
	{
		$dtools = new CDateFormatter('id_ID');
		$this->date_schedule = $dtools->format('eeee / d MMM yyyy', $this->schedule);
		$this->time_schedule =  date('H:i', strtotime($this->schedule));

		return parent::afterFind();
	}
	public function getByPage($pageNumber, $limitRow, $startInterval, $endInterval)
	{
		$offset = ($pageNumber - 1) * $limitRow;

		$criteria = new CDbCriteria();

		if (! is_null($startInterval) && ! is_null($endInterval)) {
			$criteria->addCondition('schedule >= DATE_ADD(CURDATE(), INTERVAL :date_from DAY) and schedule <= DATE_ADD(CURDATE(), INTERVAL :date_to DAY)');
			$criteria->params[':date_from'] = $startInterval;
			$criteria->params[':date_to']   = $endInterval+1;
		}
		$criteria->limit = $limitRow;
		$criteria->offset = $offset;
		$criteria->order = 'schedule ASC';

		return $this->findAll($criteria);
	}
	public function todayText() 
	{
		return date('d-m-Y', strtotime($this->schedule)) == date('d-m-Y') ? 'today' : null;
	}
}
