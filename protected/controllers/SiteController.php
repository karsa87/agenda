<?php

class SiteController extends Controller
{
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}
	
	public function accessRules()
	{
		return array(
			array('allow',  
				'actions'=>array('index','error','login','logout'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('dashboard'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		$this->layout = 'frontend';
		$this->render('index', array());
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login()) {
				User::markLogin(Yii::app()->user->id);
				
				$returnUrlArray = explode('/',Yii::app()->user->returnUrl);

				if(end($returnUrlArray)=='index.php'){
    	            $this->redirect(array('site/dashboard'));
	            }else{
	                 $this->redirect(Yii::app()->user->returnUrl);
	            }

			}
		}
		// display the login form
		$this->layout = 'login';
		$this->render('login',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		// $this->redirect(Yii::app()->homeUrl);'index.php?r=site/dashboard'
		$this->redirect(array('site/login'));
	}
	public function actionDashboard()
	{	
		$objTempatTerbanyak=TempatTerbanyak::model()->findAll();
		$objGrafikPerBulan=GrafikPerBulan::model()->findAll();

		$this->render('dashboard', array(
			'objTempatTerbanyak'=> $objTempatTerbanyak,
			'objGrafikPerBulan' => $objGrafikPerBulan,
		));
	}
}