<?php

class SettingController extends Controller
{
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}
	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'	=> array('frontend'),
				'users'		=> array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'	=> array('index'),
				'users'		=> array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionFrontend()
	{
		$modelSetting = Setting::getDefault();

		$respone = array(
			'text_footer' => $modelSetting->text_footer, 
			"interval_time"=>$modelSetting->interval_time
		);

		header('Content-type: application/json');
		print_r(CJSON::encode($modelSetting));die();
		echo CJSON::encode($respone);
		Yii::app()->end();
	}

	public function actionIndex()
	{
		$model = Setting::getDefault();

		$columnTemplate = array(
			'name' => 'Acara',
			'schedule' => 'Tanggal',
			'date_schedule' => 'Tanggal Acara',
			'time_schedule' => 'Jam Acara',
			'place' => 'Tempat',
			'surat' => 'No. Surat',
			'tgl_surat' => 'Tgl. Surat',
			'disposisi_nama' => 'Disposisi Nama',
			'disposisi_jabatan' => 'Disposisi Jabatan',
			'remark' => 'Keterangan',
		);
		
		$categoryList = CHtml::listData(Info::model()->categoryList()->findAll(),'category','category');

		if(isset($_POST['Setting']))
		{	
			$model->column_list = array();
			$model->sidebar_list = array();
			$model->attributes = $_POST['Setting'];
			for($i=1;$i<=count($columnTemplate);$i++) {
				if (! empty($_POST['kolom'.$i])) {
					$kolom = $_POST['kolom'.$i];
					$alias = empty($_POST['alias'.$i]) ? $columnTemplate[$kolom] : $_POST['alias'.$i];
					$model->column_list[$kolom] = $alias;
				}
			}

			if($model->save()) {
				Yii::app()->user->setFlash('success', 'Pengaturan telah diperbarui');
				$this->redirect(array('index'));
			}
		};

		$kolomArr=array_keys($model->column_list);
		$aliasArr=array_values($model->column_list);

		$this->render('update',array(
			'model'=>$model,
			'categoryList'	=> $categoryList,
			'dateToList' 	=> $model->dateToList(),
			'dateFromList' 	=> $model->dateFromList(),
			'kolom'			=> $columnTemplate,
			'kolomArr' 		=> $kolomArr,			
			'aliasArr'		=> $aliasArr
		));
	}
}
