<?php

class EventController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	// public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array(
				'allow',
				'actions'=>array('list'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('index', 'create','update', 'delete', 'fetchPlace','export', 'toggle'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Event;

		if(isset($_POST['Event']))
		{
			$model->attributes=$_POST['Event'];
			if($model->save())
				$this->redirect(array('index'));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		if(isset($_POST['Event']))
		{
			$model->attributes=$_POST['Event'];
			if($model->save())
				$this->redirect(array('index'));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via index grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new Event('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Event']))
			$model->attributes=$_GET['Event'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Event the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Event::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Event $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='event-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	public function actionFetchPlace($term)
	{				
		$criteria = new CDbCriteria();
		$criteria->select = 'DISTINCT place AS place';
		$criteria->addCondition("place LIKE '%".$term."%'");
		$list = Event::model()->findAll($criteria);
		$result = array();
		if (! empty($list)) {
			foreach ($list as $row) {
				array_push($result, $row->place);
			}
		} 
		echo CJSON::encode($result);
	}
	public function actionList()
	{
		/*
		{
		    "header": ["No Surat", "Acara", "Hari/Tanggal", "Tempat", "Waktu", "Disposisi"],
		    "datum": [{
		        "data": ["No1.00200e", "Seminar Kebersihan", "Senin / 22-01-2016", "Alun-Alun", "19:00", "Mochamad Karsa"],
		        "event": ""
		    }, {
		        "data": ["No1.00200e", "Seminar Kesehatan", "Selasa / 25-01-2016", "Balai Kota", "09:00", "Andre"],
		        "event": "today"
		    }, {
		        "data": ["No1.00200e", "Makan - makan", "Selasa / 25-01-2016", "Hotal Ibis", "18:00", "Andre"],
		        "event": "today"
		    }, {
		        "data": ["No1.00200e", "Seminar Keagamaan", "Rabu / 23-01-2016", "Masjid Jamek Malang", "18:00", "Kiko Insa"],
		        "event": ""
		    }, {
		        "data": ["No1.00200e", "Apel", "23-01-2016", "Kamis / Balai Kota", "18:00", "Kiko Insa"],
		        "event": ""
		    }, {
		        "data": ["No1.00200e", "Seminar Keagamaan", "Jumat / 23-01-2016", "Masjid Jamek Malang", "18:00", "Kiko Insa"],
		        "event": ""
		    }],
		    "page_now": 1
		}
		*/
		/*
		{
		    "column": ["surat" => "No Surat", "name" => "Acara", "schedule", "place", "time", "disposisi_jabatan"]
		}
		*/

		/********
		 * Keterangan :
		 * untuk date_to
		 * 0 = Hari ini
		 * 1 = Besok
		 * 2 = Lusa
		 * 7 = Minggu depan
		 * 30 = Bulan Depan
		 * untuk date_from
		 * 0 = Hari ini
		 * -1 = Kemarin
		 * -2 = Kemarin lusa
		 * -7 = Minggu kemarin
		 * -30 = Bulan kemarin
		 *******/


		$modelSetting = Setting::getDefault();

		$criteria = new CDbCriteria();
		if (! is_null($modelSetting->date_from) && ! is_null($modelSetting->date_to)) {
			$criteria->addCondition('schedule >= DATE_ADD(CURDATE(), INTERVAL :date_from DAY) and schedule <= DATE_ADD(CURDATE(), INTERVAL :date_to DAY)');
			$criteria->params[':date_from'] = $modelSetting->date_from;
			$criteria->params[':date_to']   = $modelSetting->date_to + 1;
		}
		$dataCount = Event::model()->count($criteria);
		/****
		 * Paging
		 ******/
		$halaman=ceil($dataCount / $modelSetting->limit_row);
		$page = (isset($_GET['page'])) ? (int)$_GET['page'] : 1;
		if($page >= $halaman){
			$page = 1;
		}

		$list = Event::model()->getByPage(
			$page
			, $modelSetting->limit_row
			, $modelSetting->date_from
			, $modelSetting->date_to
		);
		$dataList = array();
		$columnHeader = CJSON::decode($modelSetting->limit_column);
		if (! empty($list)) {
			foreach ($list as $event) {
				$arrayValues = array();
				foreach ($columnHeader as $field => $alias) {
					array_push($arrayValues, $event->{$field});
				}
				array_push($dataList, array(
					'data'=>$arrayValues,
					'event'=>$event->todayText(),
				));
			}
		}

		header('Content-type: application/json');

    	$response = array(
    		'header'  =>array_values($columnHeader),
    		'datum'   =>$dataList,
    		'page_now'=>$page,
    	);
    	echo CJSON::encode($response);
		Yii::app()->end();
	}
	public function actionExport($surat=null,$name=null,$schedule=null,$place=null,$disposisi_nama=null,$disposisi_jabatan=null)
	{
	    $model = new Event();
	    $dataProvider = $model->search();
	    if (!empty($surat)) {
	    	$dataProvider->criteria->addCondition("surat LIKE '%".$surat."%'");
		}
		if (!empty($name)) {
			echo $name;
			$dataProvider->criteria->addCondition("name LIKE '%".$name."%'");			
		}
		if (!empty($schedule)) {
			$dataProvider->criteria->addCondition("schedule = '".$schedule."'");			
		}
		if (!empty($place)) {
			$dataProvider->criteria->addCondition("place LIKE '%".$place."%'");			
		}
		if (!empty($disposisi_nama)) {
			$dataProvider->criteria->addCondition("disposisi_nama LIKE '%".$disposisi_nama."%'");			
		}
		if (!empty($disposisi_jabatan)) {
			$dataProvider->criteria->addCondition("disposisi_jabatan LIKE '%".$disposisi_jabatan."%'");			
		}
		$this->widget('ext.eexcelview.EExcelView', 
	    	array(
	        	'grid_mode'=>'export',
	        	'title' => 'Daftar Acara',
				'dataProvider' => $dataProvider,
				'filter' => $model,
				'columns' => 
					array(
						'surat',
						'tgl_surat',
						'name',
						'schedule',
						'place',
						'disposisi_nama',
						'disposisi_jabatan',
					),
			)
		);
		
	}

	public function actionToggle($id)
	{  	
		$model = Event::model()->findByPk($id);
		$aktif = $model->is_active;
		
		if($aktif==1) {
			$model->is_active = 0;
			$model->save();
		} else {
			$model->is_active = 1;
			$model->save();
		}
		$this->redirect(array('index'));
	}
}
