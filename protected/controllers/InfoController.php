<?php

class InfoController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request 
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  
				'actions'=>array('list'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update', 'index', 'view', 'delete', 'fetchCategory', 'fetchTitle','toggle'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Info;

		if(isset($_POST['Info']))
		{
			$model->attributes=$_POST['Info'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		if(isset($_POST['Info']))
		{
			$model->attributes=$_POST['Info'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new Info('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Info']))
			$model->attributes=$_GET['Info'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Info the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Info::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Info $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='info-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	public function actionFetchCategory()
	{
		$criteria = new CDbCriteria();
		$criteria->select = 'DISTINCT category AS category';
		$criteria->compare('category', $_GET['term'], true);
		$list = Info::model()->findAll($criteria);
		$result = array();
		if (! empty($list)) {
			foreach ($list as $row) {
				array_push($result, $row->category);
			}
		}

		echo CJSON::encode($result);
	}
	public function actionFetchTitle()
	{
		$criteria = new CDbCriteria();
		$criteria->select = 'DISTINCT title AS title';
		$criteria->compare('title', $_GET['term'], true);
		$list = Info::model()->findAll($criteria);
		$result = array();
		if (! empty($list)) {
			foreach ($list as $row) {
				array_push($result, $row->title);
			}
		}

		echo CJSON::encode($result);
	}


	public function actionList(){
		$modelSetting = Setting::getDefault();
		$sidebar = $modelSetting->sidebar;

		$modelInfo = Info::model()->randomize($sidebar);
		$respone = array(
			'info' => array(
				'title'=>$modelInfo->title,
				'content'=>$modelInfo->content,
				'image_path'=>$modelInfo->image_path
			)
		);

		if(count($modelInfo) >= 1){
			header('Content-type: application/json');
			echo CJSON::encode($respone);
			Yii::app()->end();
		}else{
			throw new CHttpException("Data Kosong", 1);
		}
    }

	public function actionToggle($id)
	{  	
		$model = Info::model()->findByPk($id);
		$aktif = $model->is_active;

		if($aktif==1) {
			Info::model()->updateByPk($id, array('is_active' => 0 ));
		} else {
			Info::model()->updateByPk($id, array('is_active' => 1 ));
		}

		$this->redirect(array('index'));
	}
}
