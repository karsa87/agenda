<?php
/* @var $this SiteController */
$this->breadcrumbs=array(
	'Dashboard	',
);

$this->pageTitle=Yii::app()->name;

?>

<style type="text/css" media="screen">
.legend div, .legend table {max-width: 50%;}	
</style>
<div class="widget-box">
	<div class="widget-header widget-header-flat widget-header-small">
		<h5 class="widget-title">
			<i class="ace-icon fa fa-signal"></i>
			Tempat Acara Terbanyak
		</h5>
	</div>
	<div class="widget-body">
		<div class="widget-main">
			<div id="piechart-placeholder">
				
			</div>
		</div><!-- /.widget-main -->
	</div><!-- /.widget-body -->
</div><!-- /.widget-box -->
<div class="widget-box">
	<div class="widget-header widget-header-flat">
		<h4 class="widget-title lighter">
			<i class="ace-icon fa fa-signal"></i>
			Jumlah Acara per Bulan
		</h4>
	</div>

	<div class="widget-body">
		<div class="widget-main padding-4">
			<div id="sales-charts"></div>
		</div><!-- /.widget-main -->
	</div><!-- /.widget-body -->
</div><!-- /.widget-box -->
<!-- inline scripts related to this page -->

<?php
$dataTempatTerbanyak='';
$warna = array("#68BC31","#2091CF","#AF4E96","#DA5430","#FEE074");
$i=0;
for($i=0;$i<count($objTempatTerbanyak);$i++){
	if($i>=5){
		break;
	}

	$dataTempatTerbanyak = $dataTempatTerbanyak.'{label: "'.$objTempatTerbanyak[$i]->place.'",  '.'data: '.$objTempatTerbanyak[0]->jml_tempat.', color: "'.$warna[$i].'"},';
}
$dataTempat = '['.$dataTempatTerbanyak.']';

$dataArrayBulan = array();
foreach ($objGrafikPerBulan as $key => $value) {
	array_push($dataArrayBulan, array($value->bulan,$value->kali));
	// $dataBulan .="[$value->bulan, $value->kali],";
}
$dataBulan = json_encode($dataArrayBulan);

$scripts=<<<EOL
	jQuery(function($) {	
	var placeholder = $('#piechart-placeholder').css({'width':'90%' , 'min-height':'150px'});
	var data = $dataTempat;
	function drawPieChart(placeholder, data, position) {
		$.plot(placeholder, data, {
			series: {
				pie: {
					show: true,
					tilt:0.8,
					highlight: {
						opacity: 0.25
					},
					stroke: {
						color: '#fff',
						width: 2
					},
					startAngle: 2
				}
			},
			legend: {
				show: true,
				position: position || "ne", 
				labelBoxBorderColor: null,
				margin:[-30,15]
			}
			,
			grid: {
				hoverable: true,
				clickable: true
			}
		})
	}
	drawPieChart(placeholder, data);
	
	 /**
	 we saved the drawing function and the data to redraw with different position later when switching to RTL mode dynamically
	 so that's not needed actually.
	 */
	placeholder.data('chart', data);
	placeholder.data('draw', drawPieChart);

	//pie chart tooltip example
	var previousPoint = null;
	var d1 = $dataBulan;
	var sales_charts = $('#sales-charts').css({'width':'100%' , 'height':'220px'});
	$.plot("#sales-charts", [
		{ label: "Acara", data: d1 },
	], {
		hoverable: true,
		shadowSize: 0,
		series: {
			lines: { show: true },
			points: { show: true }
		},xaxis: {
			tickLength: 1,
			min: 0,
			max: 12,
		},yaxis: {
			ticks: 10,
			min: 0,
			max: 30,
			tickDecimals: 0
		},grid: {
			backgroundColor: { colors: [ "#fff", "#fff" ] },
			borderWidth: 1,
			borderColor:'#555'
		}
	});
})
EOL;

/*****
 * JavaScript
 *****/
Yii::app()->clientScript->registerScript('create-event', $scripts, CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl ."/backend/js/jquery.bootstrap-duallistbox.min.js", CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl ."/backend/js/jquery.easypiechart.min.js", CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl ."/backend/js/jquery.flot.min.js", CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl ."/backend/js/jquery.flot.pie.min.js", CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl ."/backend/js/jquery.flot.resize.min.js", CClientScript::POS_END);