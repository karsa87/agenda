<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle='Masuk';
$this->breadcrumbs=array(
	'Login',
);
?>

<!-- <p class="alert alert-block alert-success">Please fill out the following form with your login credentials:</p> -->

<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'login-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
		'class' => 'form-horizontal'
	),
)); ?>

	<label class="block clearfix">
		<span class="block input-icon input-icon-right">
			<?php 
				echo $form->textField($model,'username',
					array(
						'class'=>'form-control', 
						'placeholder'=>'Username'
					)
				); 
			?>
			<i class="ace-icon fa fa-user"></i>
		</span>
		<?php echo $form->error($model,'username'); ?>
	</label>

	<label class="block clearfix">
		<span class="block input-icon input-icon-right">
			<?php 
				echo $form->passwordField($model,'password',
					array(
						'class'=>'form-control',
						'placeholder'=>'Password'
					)
				); 
			?>
			<i class="ace-icon fa fa-lock"></i>
		</span>
		<?php echo $form->error($model,'password'); ?>
	</label>

	<div class="clearfix">
		<label class="inline">
			<?php 
				echo $form->checkBox($model,'rememberMe',
					array(
						'class'=>'ace'
					)
				); 
			?>
			<span class="lbl"><?php echo $form->label($model,'rememberMe'); ?></span>
			<?php echo $form->error($model,'rememberMe'); ?>
		</label>
		<?php 
			echo CHtml::submitButton('Masuk',
				array(
					'class'=>'width-35 pull-right btn btn-sm btn-primary',
				)
			); 
		?>
	</div>

<?php $this->endWidget(); ?>
</div><!-- form -->
