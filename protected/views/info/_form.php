<?php
/* @var $this InfoController */
/* @var $model Info */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'info-form',
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array(
		'enctype'=>'multipart/form-data',
		'class'=>"form-horizontal"
	),
)); ?>

	<p class="alert alert-block alert-success">Kolom dengan <span class="required">*</span> harus diisi</p>

	<?php $errornya= $form->errorSummary($model); 
	if (strlen($errornya)>1) { ?>
		<div class="alert alert-danger">
			<button type="button" class="close" data-dismiss="alert">
				<i class="ace-icon fa fa-times"></i>
			</button>
			<?php echo $form->errorSummary($model); ?>
			<br />
		</div>	
	<?php	} ?>
	
	<div class="form-group">
		<label class="col-sm-3 control-label no-padding-right" for="category"> <?php echo $form->labelEx($model,'category'); ?> </label>
		<div class="col-sm-9">
			<?php $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
			    'model'=>$model,
			    'attribute'=>'category',
			    'sourceUrl'=>Yii::app()->createUrl('info/fetchCategory'),
				    'options'=>array(
				        'minLength'=>'2',
				    ),
				    'htmlOptions'=>array('size'=>60,'maxlength'=>100, 'required'=>'required'),
			)); ?>
			<?php echo $form->error($model,'category'); ?>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-3 control-label no-padding-right" for="title"> <?php echo $form->labelEx($model,'title'); ?> </label>
		<div class="col-sm-9">
			<?php $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
			    'model'=>$model,
			    'attribute'=>'title',
			    'sourceUrl'=>Yii::app()->createUrl('info/fetchTitle'),
				    'options'=>array(
				        'minLength'=>'2',
				    ),
				    'htmlOptions'=>array('size'=>60,'maxlength'=>150, 'required'=>'required'),
			)); ?>
			<?php echo $form->error($model,'title'); ?>
		</div>
	</div>
	
	<div class="form-group">
		<label class="col-sm-3 control-label no-padding-right" for="content"> <?php echo $form->labelEx($model,'content'); ?> </label>
		<div class="col-sm-9">
			<?php echo $form->textArea($model,'content',array('rows'=>6, 'cols'=>50, 'maxlength'=>'250','required'=>'required')); ?>
			<?php echo $form->error($model,'content'); ?>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-3 control-label no-padding-right" for="id-input-file-2"> <?php echo $form->labelEx($model,'image_file'); ?> </label>
		<div class="col-sm-9">
		    <?php echo $form->fileField($model,'image_file',array('id'=>'id-input-file-2')); ?>
			<?php echo $form->error($model,'image_file'); ?>
		</div>
	</div>

	<?php if (! empty($model->image_path)) : ?>
		<div class="form-group">
			<label class="col-sm-3 control-label no-padding-right" for="image_path">  </label>
			<div class="col-sm-9">
				<?php 
					echo CHtml::image(
						Yii::app()->request->baseUrl.'/'.$model->image_path, 
						$model->title, 
						array(
							'width'=>'90%'
						)
					); 
				?>
			</div>
		</div>
	<?php endif; ?>

	<div class="clearfix form-actions">
		<div class="col-md-offset-3 col-md-9">
			<?php echo CHtml::submitButton($model->isNewRecord ? ' Tambahkan' : '  Simpan', 
			array('class' => 'btn btn-primary', 'type' => 'submit')); ?>
		</div>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->

<?php

$scripts=<<<EOL
$('#id-input-file-2').ace_file_input({
	no_file:'No File ...',
	btn_choose:'Choose',
	btn_change:'Change',
	droppable:false,
	onchange:null,
	thumbnail:false, //| true | large
});
EOL;

Yii::app()->clientScript->registerScript('create-event', $scripts, CClientScript::POS_END);