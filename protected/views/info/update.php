<?php
/* @var $this InfoController */
/* @var $model Info */

$this->breadcrumbs=array(
	'Himbauan'=>array('index'),
	'Rubah',
);

$this->menu=array(
	array('label'=>'List Info', 'url'=>array('index')),
	array('label'=>'Create Info', 'url'=>array('create')),
);
?>

<h1>Rubah Himbauan</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>