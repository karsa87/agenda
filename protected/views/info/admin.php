<?php
/* @var $this InfoController */
/* @var $model Info */

$this->breadcrumbs=array(
	'Himbauan'=>array('index'),
	'Daftar',
);

$this->menu=array(
	array('label'=>'Create Info', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-form form').submit(function(){
	$('#info-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
$title = "Daftar Himbauan";
?>

<h1><?php echo $title; ?></h1>
<div class="table-responsive">
	<?php $this->widget('zii.widgets.grid.CGridView', array(
		'id'=>'info-grid',
		'dataProvider'=>$model->search(),
		'itemsCssClass'=>'table table-striped table-bordered table-hover',
		'enableSorting'=>true,
		'filter'=>$model,
		'columns'=>array(
			'category',
			'title',
			'content',
			array(
				'type'=>'raw',
				'name'=>'is_active',
				'header' => 'Aktif',
				'value'=>'sprintf(\'<input onchange="toggleAktif(this.checked,this.value);" type="checkbox" class="ace ace-switch ace-switch-5 toggle-status" %s data-href="%s" value="%s"  /><span class="lbl"></span>\', ($data->is_active == 1 ? \'checked="checked"\' : ""), Yii::app()->createUrl("info/toggle", array("id"=>$data->id, "f"=>$data->is_active)), $data->id)',
				'filter'=>array('1'=>'Ya','0'=>'Tidak'),
			),
			array(
				'class'=>'CButtonColumn',
				'template'=>'{view}{update}{delete}',
				'htmlOptions'=>array('class'=>'col-xs-2 col-sm-2 col-md-2'),
			    'buttons'=>array (
			        'update'=> array(
			            'label'=>'<i class="fa fa-pencil"></i>',
			            'imageUrl'=>false,
			            'options'=>array( 'title'=> 'Ubah', 'class'=>'btn btn-warning btn-minier' ),
			        ),								        
			        'delete'=>array(
			            'label'=>'<i class="fa fa-remove"></i>',
			            'imageUrl'=>false,
			            'options'=>array( 'title'=> 'Hapus', 'class'=>'btn btn-danger btn-minier' ),
			        ),
			        'view'=>array(
			            'label'=>'<i class="fa fa-eye"></i>',
			            'imageUrl'=>false,
			            'options'=>array( 'title'=> 'View', 'class'=>'btn btn-info btn-minier' ),
			        ),
			    ),
			),
		),
	)); ?>
</div>

<script type="text/javascript">
function toggleAktif(cek,idnya){
	var url = "<?php echo Yii::app()->getBaseUrl(true); ?>/index.php?r=info/toggle&id="+idnya;
	//alert(url);
    window.location.assign(url);
}
</script>