<?php
/* @var $this InfoController */
/* @var $model Info */

$this->breadcrumbs=array(
	'Himbauan'=>array('index'),
	'Tambah',
);

$this->menu=array(
	array('label'=>'List Info', 'url'=>array('index')),
);

$title = "Tambah Himbauan";
?>

<h1><?php echo $title;?></h1>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>