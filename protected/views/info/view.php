<?php
/* @var $this InfoController */
/* @var $model Info */

$this->breadcrumbs=array(
	'Himbauan'=>array('index'),
	$model->title,
);

$this->menu=array(
	array('label'=>'List Info', 'url'=>array('index')),
	array('label'=>'Create Info', 'url'=>array('create')),
	array('label'=>'Update Info', 'url'=>array('update', 'id'=>$model->id)),
);
?>

<style type="text/css">
	.profile-picture img{
		width: 100%;
	}
</style>

<div class="tab-pane in active" id="home">
	<div class="row">
		<div class="col-xs-12 col-sm-3 center">
			<span class="profile-picture">
				<?php echo empty($model->image_path) ? 'Not set' : CHtml::image(Yii::app()->request->baseUrl.'/'.$model->image_path, $model->title);?>
			</span>
		</div><!-- /.col -->

		<div class="col-xs-12 col-sm-9">
			<h4 class="blue">
				<span class="middle"><?php echo $model->title;?></span>
				<?php 
					if($model->is_active == 1){ ?>
						<span class="label label-success arrowed">
							<i class="ace-icon glyphicon glyphicon-ok smaller-80 align-middle"></i>
							Aktif
						</span>
				<?php
					}else{ ?>
						<span class="label">
							<i class="ace-icon glyphicon glyphicon-remove  smaller-80 align-middle"></i>
							Tidak Aktif
						</span>
				<?php
					}
				?>
			</h4>

			<div class="profile-user-info">
				<div class="profile-info-row">
					<div class="profile-info-name"> Category </div>

					<div class="profile-info-value">
						<span><?php echo $model->category;?></span>
					</div>
				</div>

				<div class="profile-info-row">
					<div class="profile-info-name"> Isi Himbauan </div>

					<div class="profile-info-value">
						<span><?php echo $model->content;?></span>
					</div>
				</div>
			</div>
		</div><!-- /.col -->
	</div><!-- /.row -->
</div>