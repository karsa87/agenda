<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs=array(
	'User'=>array('index'),
	'Tambah',
);

$this->menu=array(
	array('label'=>'List User', 'url'=>array('index')),
);
?>

<h1>Tambah User</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>