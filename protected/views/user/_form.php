<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'user-form',
	'enableAjaxValidation'=>false,
	'htmlOptions'=> [
		'class' => 'form-horizontal'
	]
)); ?>

	<p class="alert alert-block alert-success">Kolom dengan <span class="required">*</span> harus diisi.</p>

	<?php $errornya= $form->errorSummary($model); 
		if (strlen($errornya)>1) { ?>
			<div class="alert alert-danger">
				<button type="button" class="close" data-dismiss="alert">
					<i class="ace-icon fa fa-times"></i>
				</button>
				<?php echo $form->errorSummary($model); ?>
				<br />
			</div>	
	<?php	} ?>
	<div class="form-group">
		<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> <?php echo $form->labelEx($model,'username'); ?> </label>
		<div class="col-sm-9">
			<?php 
				echo $form->textField($model,'username',
					array(
						'id'=>"form-field-3", 
						'placeholder'=>"Username", 
						'class'=>"form-control", 
						'required'=>'required', 
						'size'=>60,
						'maxlength'=>128
					)
				); 
			?>
			<?php echo $form->error($model,'username'); ?>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> <?php echo $form->labelEx($model,'full_name'); ?> </label>
		<div class="col-sm-9">
			<?php echo $form->textField($model,'full_name',array( 'id'=>"form-field-4", 'placeholder'=>"Nama Lengkap", 'class'=>"form-control", 'required'=>'required' , 'size'=>60,'maxlength'=>128)); ?>
			<?php echo $form->error($model,'full_name'); ?>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> <?php echo $form->labelEx($model,'position'); ?> </label>
		<div class="col-xs-12 col-sm-9">						
			<?php 
				echo $form->dropDownList($model,'position', 
					array(
						'IT'=>'IT', 
						'Resepsionis'=>'Resepsionis', 
						'CS'=>'CS',
						'admin'=>'admin', 
					), 
					array(
						'class'=>"select2", 
						'required'=>'required'
					)
				); 
			?>
			<?php echo $form->error($model,'position'); ?>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> <?php echo $form->labelEx($model,'password'); ?> </label>
			<div class="col-sm-9">
			<?php 
				echo $form->textField($model, 'password', 
					array( 
						'id'=>"form-field-1", 
						'placeholder'=>"Password", 
						'class'=>"form-control", 
						'size'=>50, 
						'maxlength'=>128, 
						// 'value'=>''
					)
				); 
			?>
			<?php echo $form->error($model,'password'); ?>
		</div>
	</div>

	<div class="clearfix form-actions">
					<div class="col-md-offset-3 col-md-9">
		<?php 
			echo CHtml::submitButton($model->isNewRecord ? ' Tambahkan' : '  Simpan', 
				array(
					'class' => 'btn btn-primary', 
					'type' => 'submit'
				)
			); 
		?>
		</div>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->

<?php

$scripts=<<<EOL
$('.select2').css('width','200px').addClass('tag-input-style').select2({
	allowClear:true
});
EOL;

/*******
 * JavaScript
 *******/
Yii::app()->clientScript->registerScript('create-event', $scripts, CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl ."/backend/js/select2.min.js", CClientScript::POS_END);

/*******
 * Css
 *******/
Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl ."/backend/css/select2.min.css");
