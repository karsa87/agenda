<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs=array(
	'User'=>array('index'),
	'Daftar',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-form form').submit(function(){
	$('#user-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");

$title = "Daftar User";
?>

<h1><?php echo $title; ?></h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'user-grid',
	'dataProvider'=>$model->search(),
	'itemsCssClass'=>'table table-striped table-bordered table-hover',
	'enableSorting'=>true,
	'filter'=>$model,
	'columns'=>array(
		'full_name',
		'username',
		'position',
		array('name'=> 'last_login',
			'header' => 'Login terakhir',
			'value' => 'date("d/m/Y H:i:s", strtotime($data->last_login))' 
		),
		array(
			'type'=>'raw',
			'name'=>'is_active',
			'header' => 'Aktif',
			'value'=>'sprintf(\'<input  onchange="toggleAktif(this.checked,this.value);" type="checkbox" class="ace ace-switch ace-switch-5 toggle-status" %s data-href="%s"  value="%s"  /><span class="lbl"></span>\', ($data->is_active == 1 ? \'checked="checked"\' : ""), Yii::app()->createUrl("user/toggle", array("id"=>$data->id, "f"=>$data->is_active)), $data->id)',
			'filter'=>array('1'=>'Ya','0'=>'Tidak'),
		),
		array(
				'class'=>'CButtonColumn',
									'template'=>'{update} {delete}',
									'htmlOptions'=>array('class'=>'col-xs-2 col-sm-2 col-md-1'),
								    'buttons'=>array (
								        'update'=> array(
								            'label'=>'<i class="fa fa-pencil"></i>',
								            'imageUrl'=>false,
								            'options'=>array( 'title'=> 'Ubah', 'class'=>'btn btn-warning btn-xs' ),
								        ),								        
								        'delete'=>array(
								            'label'=>'<i class="fa fa-remove"></i>',
								            'imageUrl'=>false,
								            'options'=>array( 'title'=> 'Hapus', 'class'=>'btn btn-danger btn-xs' ),
								        ),
								    ),
			),
	),
)); ?>

<script type="text/javascript">
function toggleAktif(cek,idnya){
	var url = "<?php echo Yii::app()->getBaseUrl(true); ?>/index.php?r=user/toggle&id="+idnya;
	//alert(url);
    window.location.assign(url);
}
</script>