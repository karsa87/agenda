<?php
/* @var $this EventController */
/* @var $model Event */
	$this->breadcrumbs=array(
		'Pengaturan',
	);
?>

<h1>Pengaturan Aplikasi</h1>

<?php $this->renderPartial('_form', array(
		'model'=>$model, 
		'categoryList'	=> $categoryList, 
		'dateToList' 	=> $dateToList,
		'dateFromList' 	=> $dateFromList,
		'kolom'			=> $kolom,
		'kolomArr' 		=> $kolomArr,
		'aliasArr'		=> $aliasArr
	)); ?>