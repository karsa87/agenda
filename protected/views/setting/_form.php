<?php
/* @var $this EventController */
/* @var $model Event */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'setting-form',
	'enableAjaxValidation'=>false,
	'htmlOptions' => array(
        'enctype' => 'multipart/form-data',
        'class'=>"form-horizontal"
    ),
)); ?>
	<?php if (Yii::app()->user->hasFlash('success')) : ?>
		<div class="alert alert-success">
			<button type="button" class="close" data-dismiss="alert">
				<i class="ace-icon fa fa-times"></i>
			</button>
			<?php echo Yii::app()->user->getFlash('success'); ?>
		</div>	
	<?php endif; ?>
	<?php if ($model->hasErrors()) : ?>
		<div class="alert alert-danger">
			<button type="button" class="close" data-dismiss="alert">
				<i class="ace-icon fa fa-times"></i>
			</button>
			<?php echo $form->errorSummary($model); ?>
		</div>	
	<?php endif; ?>
	
	<div class="form-group">
		<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Agenda tampil mulai </label>
		<div class="col-sm-9">
			<?php 
				echo $form->dropDownList($model,'date_from', $dateFromList,
					array(
						'class'=>'select2',
						'prompt'=>'Semua',
					)
				); 
			?>
			<?php echo $form->error($model,'date_from'); ?>
			Sampai
			<?php 
				echo $form->dropDownList($model, 'date_to', $dateToList, 
					array(
						"class"=>"select2",
						'prompt'=>'Semua',
					)
				); 
			?>
			<?php echo $form->error($model,'date_to'); ?>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-3 control-label no-padding-right" for="form-field-1">Jumlah kegiatan / halaman</label>
		<div class="col-sm-9">
		        <?php 
		        	echo $form->textField($model,'limit_row',
		        		array(
		        			'size'=>5,
		        			'maxlength'=>128, 
		        			'class'=>'knob',
		        			'data-min'=>'0',
		        			'data-max'=>'10',
		        			'data-width'=>'100',
		        			'data-height'=>'100',
		        			'data-thickness'=>'.2'
		        		)
		        	); 
		        ?>
		        <?php echo $form->error($model,'limit_row'); ?>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Pengaturan Kolom </label>
		<div class="col-sm-9">
			<table>
				<thead>
					<tr>
						<th>No</th>
						<th>Kolom</th>
						<th>Alias</th>
					</tr>
				</thead>
				<tbody>				
					<?php for($i=1;$i<=8;$i++) { ?>
					<tr>
						<td style="padding-right:10px;padding-bottom:10px;"><?php echo $i; ?></td>
						<td style="padding-right:10px;padding-bottom:10px;"><?php 
							if(isset($kolomArr[$i-1])) {
								echo CHtml::dropDownList('kolom'.$i, $kolomArr[$i-1], $kolom, array('empty'=>'Pilih',"class"=>"select2")); 
							} else {
								echo CHtml::dropDownList('kolom'.$i, null, $kolom, array('empty'=>'Pilih', "class"=>"select2"));
							} 

							?></td>
						<td style="padding-right:10px;padding-bottom:10px;"><input type="text" name="alias<?php echo $i; ?>" 
							value="<?php if(isset($aliasArr[$i-1])) { echo $aliasArr[$i-1];	} ?>" placeholder="Alias"></td>					
					</tr>
					<?php } ?>
				</tbody>
			</table>
		</div>
	</div>

	<div class="row">
		
	</div>

	<div class="form-group">
		<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Kategori Himbauan</label>
		<div class="col-sm-9">
		        <?php echo $form->dropDownList($model,'sidebar_list', $categoryList, 
		        array('size' => 10, 'multiple'=>true, 'class'=>'form-control chosen-select')); ?>
		        <?php echo $form->error($model,'sidebar_list'); ?>
		        <span class="help-block">Kosongi untuk menampilkan seluruh kategori</span>
		</div>
	</div>


	<div class="form-group">
		<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Teks Berjalan </label>
		<div class="col-sm-9">
			<?php echo $form->textArea($model,'text_footer',array('rows'=>3, 'cols'=>60, 'required'=>'required')); ?>
			<?php echo $form->error($model,'text_footer'); ?>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Periode Refresh </label>
		<div class="col-sm-9">
		        <?php 
		        	echo $form->textField($model,'interval_time',
		        		array(
		        			'size'=>5,
		        			'maxlength'=>128, 
		        			'class'=>'knob',
		        			'data-min'=>'0',
		        			'data-max'=>'60',
		        			'data-width'=>'100',
		        			'data-height'=>'100',
		        			'data-thickness'=>'.2'
		        		)
		        	); 
		        ?> menit
		        <?php echo $form->error($model,'interval_time'); ?>
		</div>
	</div>

	<div class="hr hr-18 dotted hr-double"></div>
	<h4 class="blue">
		<i class="ace-icon fa fa-facebook green"></i>
		Facebook
	</h4>
	<div class="hr hr-18 dotted hr-double"></div>

	<div class="form-group">
		<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> <?php echo $form->labelEx($model,'facebook'); ?> </label>
		<div class="col-sm-9">
			<?php echo $form->textField($model,'facebook',array('size'=>60,'maxlength'=>225,  'id'=>"facebook", 'placeholder'=>"Nama Facebook", 'class'=>"col-xs-12")); ?>
			<?php echo $form->error($model,'facebook'); ?>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> <?php echo $form->labelEx($model,'url_facebook'); ?> </label>
		<div class="col-sm-9">
			<?php echo $form->textField($model,'url_facebook',array('size'=>60,'maxlength'=>225,  'id'=>"url_facebook", 'placeholder'=>"URL Facebook", 'class'=>"col-xs-12")); ?>
			<?php echo $form->error($model,'url_facebook'); ?>
		</div>
	</div>

	<div class="hr hr-18 dotted hr-double"></div>
	<h4 class="blue">
		<i class="ace-icon fa fa-twitter green"></i>
		Twitter
	</h4>
	<div class="hr hr-18 dotted hr-double"></div>

	<div class="form-group">
		<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> <?php echo $form->labelEx($model,'twitter'); ?> </label>
		<div class="col-sm-9">
			<?php echo $form->textField($model,'twitter',array('size'=>60,'maxlength'=>225,  'id'=>"twitter", 'placeholder'=>"Nama Twitter", 'class'=>"col-xs-12")); ?>
			<?php echo $form->error($model,'twitter'); ?>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> <?php echo $form->labelEx($model,'url_twitter'); ?> </label>
		<div class="col-sm-9">
			<?php echo $form->textField($model,'url_twitter',array('size'=>60,'maxlength'=>225,  'id'=>"url_twitter", 'placeholder'=>"URL Twitter", 'class'=>"col-xs-12")); ?>
			<?php echo $form->error($model,'url_twitter'); ?>
		</div>
	</div>

	<div class="hr hr-18 dotted hr-double"></div>
	<h4 class="blue">
		<i class="ace-icon fa fa-envelope green"></i>
		Email
	</h4>
	<div class="hr hr-18 dotted hr-double"></div>

	<div class="form-group">
		<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> <?php echo $form->labelEx($model,'email1'); ?> </label>
		<div class="col-sm-9">
			<?php echo $form->textField($model,'email1',array('size'=>60,'maxlength'=>225,  'id'=>"email1", 'placeholder'=>"Email 1", 'class'=>"col-xs-12")); ?>
			<?php echo $form->error($model,'email1'); ?>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> <?php echo $form->labelEx($model,'email2'); ?> </label>
		<div class="col-sm-9">
			<?php echo $form->textField($model,'email2',array('size'=>60,'maxlength'=>225,  'id'=>"email2", 'placeholder'=>"Email 2", 'class'=>"col-xs-12")); ?>
			<?php echo $form->error($model,'email2'); ?>
		</div>
	</div>

	<div class="hr hr-18 dotted hr-double"></div>
	<h4 class="blue">
		<i class="ace-icon fa fa-header green"></i>
		Text Header
	</h4>
	<div class="hr hr-18 dotted hr-double"></div>

	<div class="form-group">
		<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> <?php echo $form->labelEx($model,'text_header1'); ?> </label>
		<div class="col-sm-9">
			<?php echo $form->textField($model,'text_header1',array('size'=>60,'maxlength'=>225,  'id'=>"text_header1", 'placeholder'=>"Text Header 1", 'class'=>"col-xs-12")); ?>
			<?php echo $form->error($model,'text_header1'); ?>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> <?php echo $form->labelEx($model,'text_header2'); ?> </label>
		<div class="col-sm-9">
			<?php echo $form->textField($model,'text_header2',array('size'=>60,'maxlength'=>225,  'id'=>"text_header2", 'placeholder'=>"Text Header 2", 'class'=>"col-xs-12")); ?>
			<?php echo $form->error($model,'text_header2'); ?>
		</div>
	</div>

	<div class="hr hr-18 dotted hr-double"></div>
	<h4 class="blue">
		<i class="ace-icon fa fa-feed green"></i>
		Website
	</h4>
	<div class="hr hr-18 dotted hr-double"></div>

	<div class="form-group">
		<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> <?php echo $form->labelEx($model,'website'); ?> </label>
		<div class="col-sm-9">
			<?php echo $form->textField($model,'website',array('size'=>60,'maxlength'=>225,  'id'=>"website", 'placeholder'=>"Website", 'class'=>"col-xs-12")); ?>
			<?php echo $form->error($model,'website'); ?>
		</div>
	</div>

	<div class="clearfix form-actions">
		<div class="col-md-offset-3 col-md-9">
			<?php echo CHtml::submitButton(' Simpan', 
			array('class' => 'btn btn-primary', 'type' => 'submit')); ?>
		</div>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->

<?php

$scripts=<<<EOL
$('.select2').css('width','200px').addClass('tag-input-style').select2({
	allowClear:true
});
$('.chosen-select').chosen({allow_single_deselect:true});
$(".knob").knob();
EOL;

/*******
 * JavaScript
 *******/
Yii::app()->clientScript->registerScript('create-event', $scripts, CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl ."/backend/js/select2.min.js", CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl ."/backend/js/chosen.jquery.min.js", CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl ."/backend/js/jquery.knob.min.js", CClientScript::POS_END);

/*******
 * Css
 *******/
Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl ."/backend/css/select2.min.css");
Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl ."/backend/css/chosen.min.css");
