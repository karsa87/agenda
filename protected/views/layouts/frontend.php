<html>
<head>  
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Agenda DKP</title>

    <base href="<?php echo Yii::app()->request->baseUrl; ?>/frontend/"></base>
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Slick slider css -->
    <link href="css/skdslider.css" rel="stylesheet">
    <!-- Font awesome css -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <!-- smooth animate css file -->
    <link rel="stylesheet" href="css/animate.css"> 
    <!-- clock animate css file -->
    <link rel="stylesheet" href="css/clock.css" type="text/css"> 
    <!-- Change Background -->
    <link rel="stylesheet" href="css/change_background.css" type="text/css">
    <!-- Main style css -->
    <link rel="stylesheet" href="css/style.css">
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/png" href="img/favicon.png"/>
        <!-- Main style css -->
    <link rel="stylesheet" href="css/mobilestyle.css">
</head>
<body>
  <div id="container-background">
      <img src="img/hijau/b1.jpg">
      <img src="img/hijau/b2.jpg">
      <img src="img/hijau/b3.jpg">
      <img src="img/hijau/b4.jpg">
      <img src="img/hijau/b5.jpg">
  </div>

  <header id="headerArea">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 hrhead">
        <div class="row">
          <div class="col-xs-2 col-sm-2">
            <div class="navbar-brand">
              <img src="img/logo_kota_malang.png" alt="logo">
            </div>
          </div>
          <div class="col-xs-10 col-sm-10">
            <div class="navbar-title">

            </div>
          </div>
        </div>
      </div>
      <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
        <div class="row">
          <div class="col-xs-6  col-sm-6 col-md-6 col-lg-6" id="Date"></div>
          <div class="col-xs-6  col-sm-6 col-md-6 col-lg-6 pull-right ratakanan">
            <span id="hours"></span>
            <span id="point">:</span>
            <span id="min"></span>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-12 col-sm-9">
        <div class="table-responsive">         
            <table id="dynamic-table" class="table"></table>
        </div>
      </div>
      <div class="col-sm-3">
        <div class="row">
          <div class="col-sm-12 col-xs-12">
            <div class="contentarea">
              <div class="content">
                <div class="info facebook">
                </div>
                <div class="info twitter">
                </div>
                <div class="info email1">
                </div>
                <div class="info email2">
                </div>
                <div class="info website">
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="hidden-xs slide_descright"></div>
      </div>
    </div> 
  </header>
  <!-- START FOOTER SECTION -->
  <footer id="footer">
    <div class="footer_area">
      
    </div>
  </footer>
  <!-- END FOOTER SECTION -->

  <!-- JQuery Files -->
  <!-- Clock -->
  <script type="text/javascript" src="js/jquery-1.6.4.min.js"></script>
  <!-- Initialize jQuery Library -->
  <script src="js/jquery.min.js"></script>
  <!-- Skds slider -->
  <script src="js/skdslider.js"></script>
  <!-- Bootstrap js  -->
  <script src="js/bootstrap.min.js"></script>
  <!-- For smooth animatin  -->
  <script src="js/wow.min.js"></script> 
  <!-- Jadwal Sholat -->
  <script type="text/javascript" src="js/pray_times.js"></script>

  <!-- Custom js -->
  <script type="text/javascript" src="js/custom.js"></script>

  <!-- Tambahan Table -->
  <script src="js/jquery.dataTables.min.js"></script>
  <script src="js/jquery.dataTables.bootstrap.min.js"></script>

    <!-- Clock -->
    <script type="text/javascript">
      /* INTERVAL */
      var INTERVAL_TIME = 5; //Interval dalam satuan minute
      var MINUTE;
      var MINUTE_FOOTER; //
      var INTERVAL_FOOTER_TIME = 5;//interval dalam satuan minute

      /* URL */
      var page_now = "";
      
      var TABLE_JSON_URL = "<?php echo Yii::app()->createUrl('event/list'); ?>";
      var INFO_JSON_URL = "<?php echo Yii::app()->createUrl('info/list'); ?>";
      var INFO_SETTING_JSON_URL = "<?php echo Yii::app()->createUrl('setting/frontend'); ?>";

      /* ARRAY HARI DAN BULAN */
      var monthNames = [ "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Augustus", "September", "Oktober", "November", "Desember" ]; 
      var dayNames= ["Minggu","Senin","Selasa","Rabu","Kamis","Jum'at","Sabtu"];

      /* VARIABLE TANGGAL SEKARANG */
      var today = new Date();

      $(document).ready(function() {

        /* CALL WEBSERVICE DATA TABLES */ 
        function call_datatable() {
          var url = TABLE_JSON_URL;
          if(page_now != ""){
            page_now = page_now + 1;
            url = url+"&page="+page_now;
          }

          $.get(url,{},function(response) {     
            create_table(response);
          }).error(function(){
            $(".table-responsive").append("<div class='alert'><h1>Error : Maaf tidak bisa menampilkan jadwal. Periksa ulang koneksi ulang anda</h1></div>");
          });
        }

        function create_table(datas){
          $("#dynamic-table").append('<thead class="table-new"></thead><tbody class="table-new"></tbody>');
          var thead = " ";
          var tbody = " ";
          arrayHeader = datas.header;
          arrayDatum = datas.datum;

          for(i=0;i<arrayHeader.length;i++){
            thead += '<th class="table-new">'+arrayHeader[i]+'</th>'
          }

          for(i=0;i<arrayDatum.length;i++){
            tbody +='<tr class="table-new '+arrayDatum[i].event+' ">'
            cols = arrayDatum[i].data;

            for(j=0;j<cols.length;j++){
              if(cols[j] == null){
                cols[j] = ''
              }
              tbody += '<td class="table-new">'+cols[j]+'</td>'
            }
            tbody += '</tr>';
          }

          $("#dynamic-table thead").append(thead);
          $("#dynamic-table tbody").append(tbody);
        }

        function remove_table(){
          $(".table-new").remove();
          $(".alert").remove();
        }

        function titleCase(string) { 
          string = string.replace("_"," ");
          return string.charAt(0).toUpperCase() + string.slice(1); 
        }

        /* CALL WEBSERVICE CREATE INFO */ 
        function call_info() {
          $.get(INFO_JSON_URL,{},function(response) {     
            create_info(response);
          }).error(function(){
            $(".slide_descright").append("<div class='alert'><strong>Error : Maaf tidak bisa menampilkan Info. Periksa ulang koneksi ulang anda</strong></div>");
          });
        }

        function create_info(datas){
          var div_info = "";
          row = datas.info;
          // (row.title != null) ? row.title : '';
          if(row != null){
            if(row.title == null){
              row.title = '';
            }

            if(row.content==null){
              row.content = '';
            }

            div_info += ''
            +'<div class="row div-info">'
              +'<div class="col-sm-12"> '
                +'<div class="contentarea"> '
                  +'<div class="content-header"><h2>'+row.title+'</h2></div> '  
                  +'<div class="content">'
                    +'<p>'+row.content+'</p>'
                  +'</div>'
                +'</div>'
              +'</div>'
            +'</div>';

            if(row.image_path != null){
              div_info += ''
              +'<div class="row div-info">'
                +'<div class="col-sm-12">'
                  +'<div class="header_btnarea">'
                    +'<img src="../'+row.image_path+'">'
                  +'</div>'
                +'</div>'
              +'</div>';
            }

            $(".slide_descright").append(div_info);
          }
        }

        function remove_info(){
          $(".div-info").remove();
        }

        /****
         * CALL WEBSERVICE FOOTER
         ****/
        function call_setting() {
          $.get(INFO_SETTING_JSON_URL,{},function(response) {     
            create_setting(response);
          }).error(function(){
            console.log("Sorry could not proceed");
          });
        }

        function create_setting(datas){
          INTERVAL_TIME = datas.interval_time;
          INTERVAL_FOOTER_TIME = datas.interval_time;
          var div_footer = "";
          var div_header = "";
          var text_footer = "";
          var div_facebook = "";
          var div_twitter = "";
          var div_email1 = "";
          var div_email2 = "";
          var div_website = "";
          var jadwal = create_pray_time();
          
          if(datas.text_footer != null){
            text_footer = datas.text_footer;
          }

          if(datas.text_header1 != null){
            div_header = '<div class="header"><h4>'+datas.text_header1+'</h4>';
            if(datas.text_header1 != null){
              div_header += '<h5>'+datas.text_header2+'</h5>';
            }
            div_header += '</div>';
          }

          if(datas.facebook != null){
            if(datas.url_facebook != null){
              div_facebook += '<a href="'+datas.url_facebook+'">';
            }else{
              div_facebook += '<a href="#">';
            }
            div_facebook += '<i class="ace-icon fa fa-facebook-square text-primary bigger-150"></i> '+datas.facebook;
            div_facebook += '</a>';
          }

          if(datas.twitter != null){
            if(datas.url_twitter != null){
              div_twitter += '<a href="'+datas.url_twitter+'">';
            }else{
              div_twitter += '<a href="#">';
            }

            div_twitter += '<i class="ace-icon fa fa-twitter light-blue bigger-150"></i> '+datas.twitter;
            div_twitter += '</a>';
          }

          if(datas.email1 != null){
            div_email1 = '<i class="ace-icon fa fa-envelope light-blue bigger-150"> '+datas.email1+'</i>';
          }

          if(datas.email2 != null){
            div_email2 = '<i class="ace-icon fa fa-envelope light-blue bigger-150"> '+datas.email2+'</i>';
          }

          if(datas.website != null){
            div_website = '<a href="http://'+datas.website+'"><i class="ace-icon fa fa-globe light-blue bigger-150"></i> '+datas.website+'</a>';
          }

          div_footer += '<p><marquee behavior="scroll" direction="left">'+text_footer+'&nbsp; &nbsp; &nbsp; &nbsp; Jadwal Sholat &nbsp'+jadwal+'</marquee></p>';

          $(".footer_area").append(div_footer);
          $(".navbar-title div").remove();
          $(".navbar-title").append(div_header);
          $(".facebook a").remove();
          $(".facebook").append(div_facebook);
          $(".twitter a").remove();
          $(".twitter").append(div_twitter);
          $(".email1 i").remove();
          $(".email1").append(div_email1);
          $(".email2 i").remove();
          $(".email2").append(div_email2);
          $(".website a").remove();
          $(".website").append(div_website);
        }

        function remove_info_setting(){
          $(".footer_area p").remove();
        }

        // Extract the current date from Date object
        today.setDate(today.getDate());
        // Output the day, date, month and year   
        $('#Date').html(dayNames[today.getDay()] + " " + today.getDate() + ' ' + monthNames[today.getMonth()] + ' ' + today.getFullYear());

        setInterval( function() {
          // Create a today() object and extract the seconds of the current time on the visitor's
          var seconds = new Date().getSeconds();
          // Add a leading zero to seconds value
          $("#sec").html(( seconds < 10 ? "0" : "" ) + seconds);
        },1000);
            
        setInterval( function() {
          // Create a today() object and extract the minutes of the current time on the visitor's
          var minutes = new Date().getMinutes();
          // Add a leading zero to the minutes value
          $("#min").html(( minutes < 10 ? "0" : "" ) + minutes);
          if(MINUTE != minutes){
            if ((minutes%INTERVAL_TIME)==0) {
              /* DROP AND CREATE INFO */
              remove_info();
              call_info();

              /* DROP AND CREATE TABLE DATA */
              remove_table();
              call_datatable();

              MINUTE = minutes;
            }
          }
          if(MINUTE_FOOTER != minutes){
            if((minutes%INTERVAL_FOOTER_TIME)==0){
              remove_info_setting();
              call_setting();
              MINUTE_FOOTER = minutes
            }
          }
        },1000);
            
        setInterval( function() {
          // Create a today() object and extract the hours of the current time on the visitor's
          var hours = new Date().getHours();
          // Add a leading zero to the hours value
          $("#hours").html(( hours < 10 ? "0" : "" ) + hours);
        }, 1000);

        /******
         * Create Jadwal Sholat Harian
         ********/
        function create_pray_time(){
          var text = '';
          var date = new Date(); // today
          var times = prayTimes.getTimes(date, [7.9800, 112.6200], 7.00);
          var list = ['Fajr', 'Dhuhr', 'Asr', 'Maghrib', 'Isha'];
          var list_name = ['Subuh', 'Dzuhur', 'Ashar', 'Maghrib', "Isha'"];

          var html = '<table id="timetable"><tr>';

          for(var i in list)  {
            text += list_name[i] +' : '+ times[list[i].toLowerCase()]+'&nbsp; &nbsp; ';
          }

          return text;
        }

        /***
         * ONLOAD
         ****/
         call_info();
         call_datatable();
         call_setting();
      });
      </script>
</body
</html>