<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.2.1.1.min.js"></script>
		<title><?php echo CHtml::encode($this->pageTitle); ?></title>

		<meta name="description" content="overview &amp; stats" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

		<base href="<?php echo Yii::app()->request->baseUrl; ?>/backend/"></base>
		<!-- bootstrap & fontawesome -->
		<link rel="stylesheet" href="css/bootstrap.min.css" />
		<link rel="stylesheet" href="font-awesome/4.2.0/css/font-awesome.min.css" />

		<!-- page specific plugin styles -->
		<link rel="stylesheet" href="css/jquery-ui.custom.min.css" />
		<!-- text fonts -->
		<!-- <link rel="stylesheet" href="fonts/fonts.googleapis.com.css" /> -->

		<!-- ace styles -->
		<link rel="stylesheet" href="css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />
		 <!-- Favicon -->
    	<link rel="shortcut icon" type="image/png" href="img/favicon.png"/>
		<script src="js/ace-extra.min.js"></script>
	</head>
<body  class="no-skin">
	<div id="navbar" class="navbar navbar-default">
		<script type="text/javascript">
			try{ace.settings.check('navbar' , 'fixed')}catch(e){}
		</script>

		<div class="navbar-container" id="navbar-container">
			<button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar">
				<span class="sr-only">Toggle sidebar</span>

				<span class="icon-bar"></span>

				<span class="icon-bar"></span>

				<span class="icon-bar"></span>
			</button>

			<div class="navbar-header pull-left">
				<a href="<?php echo Yii::app()->request->baseUrl; ?>" class="navbar-brand">
					<small>
						<i class="fa fa-leaf"></i>
						DKP Kota Malang
					</small>
				</a>
			</div>

			<div class="navbar-buttons navbar-header pull-right" role="navigation">
				<ul class="nav ace-nav">
					<li class="light-blue">
						<a data-toggle="dropdown" href="#" class="dropdown-toggle">
							<img class="nav-user-photo" src="avatars/user.jpg" alt="Foto <?php echo Yii::app()->user->name; ?>" />
							<span class="user-info">
								<small>Selamat datang,</small>
								<?php echo Yii::app()->user->name; ?>
							</span>

							<i class="ace-icon fa fa-caret-down"></i>
						</a>

						<ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">							

							<li class="divider"></li>

							<li>
								<a href="../index.php?r=site/logout">
									<i class="ace-icon fa fa-power-off"></i>
									Logout
								</a>
							</li>
						</ul>
					</li>
				</ul>
			</div>
		</div><!-- /.navbar-container -->
	</div>
	<div class="main-container" id="main-container">
		<script type="text/javascript">
			try{ace.settings.check('main-container' , 'fixed')}catch(e){}
		</script>

		<div id="sidebar" class="sidebar                  responsive">
			<script type="text/javascript">
				try{ace.settings.check('sidebar' , 'fixed')}catch(e){}
			</script>

			<div class="sidebar-shortcuts" id="sidebar-shortcuts">
				<div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
					<a href="../index.php?r=event/create" class="btn btn-info">
						<i class="ace-icon fa fa-pencil"></i>
					</a>

					<a href="../index.php?r=user/create" class="btn btn-warning">
						<i class="ace-icon fa fa-users"></i>
					</a>

					<a href="<?php echo Yii::app()->createUrl('setting/index'); ?>" class="btn btn-danger">
						<i class="ace-icon fa fa-cogs"></i>
					</a>
				</div>

				<div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
					<span class="btn btn-success"></span>

					<span class="btn btn-info"></span>

					<span class="btn btn-warning"></span>

					<span class="btn btn-danger"></span>
				</div>
			</div><!-- /.sidebar-shortcuts -->

			<ul class="nav nav-list">
				<li class="<?php if (empty($_GET['r']) || $_GET['r']=='site' || $_GET['r']=='site/index' ) { echo 'active';}  ?>">
					<a href="<?php echo Yii::app()->createUrl('site/dashboard'); ?>">
						<i class="menu-icon fa fa-tachometer"></i>
						<span class="menu-text"> Dashboard </span>
					</a>

					<b class="arrow"></b>
				</li>

				<li class="<?php if ( isset($_GET['r']) && ($_GET['r']=='event' || $_GET['r']=='event/index' || $_GET['r']=='event/create' ) ) { echo 'active';}  ?>">
					<a href="#" class="dropdown-toggle">
						<i class="menu-icon fa fa-desktop"></i>
						<span class="menu-text">
							Kegiatan
						</span>

						<b class="arrow fa fa-angle-down"></b>
					</a>

					<b class="arrow"></b>

					<ul class="submenu">
						<li class="<?php if (  isset($_GET['r']) && ($_GET['r']=='event' || $_GET['r']=='event/index') ) { echo 'active';}  ?>">
							<a href="<?php echo Yii::app()->createUrl('event/index'); ?>">
								<i class="menu-icon fa fa-caret-right"></i>
								Daftar Kegiatan
							</a>

							<b class="arrow"></b>
						</li>

						<li class="<?php if ( isset($_GET['r']) && ( $_GET['r']=='event/create' )) { echo 'active';}  ?>">
							<a href="<?php echo Yii::app()->createUrl('event/create'); ?>">
								<i class="menu-icon fa fa-caret-right"></i>
								Tambah Kegiatan
							</a>

							<b class="arrow"></b>
						</li>
					</ul>
				</li>

				<li class="<?php if ( isset($_GET['r']) && ($_GET['r']=='user' || $_GET['r']=='user/index' || $_GET['r']=='user/create' ) ) { echo 'active';}  ?>">
					<a href="#" class="dropdown-toggle">
						<i class="menu-icon fa fa-users"></i>
						<span class="menu-text">
							Users
						</span>

						<b class="arrow fa fa-angle-down"></b>
					</a>

					<b class="arrow"></b>

					<ul class="submenu">
						<li class="<?php if (  isset($_GET['r']) && ($_GET['r']=='user' || $_GET['r']=='user/index') ) { echo 'active';}  ?>">
							<a href="<?php echo Yii::app()->createUrl('user/index'); ?>">
								<i class="menu-icon fa fa-caret-right"></i>
								Daftar User
							</a>

							<b class="arrow"></b>
						</li>

						<li class="<?php if ( isset($_GET['r']) && ( $_GET['r']=='user/create' )) { echo 'active';}  ?>">
							<a href="<?php echo Yii::app()->createUrl('user/create'); ?>">
								<i class="menu-icon fa fa-caret-right"></i>
								Tambah User
							</a>

							<b class="arrow"></b>
						</li>
					</ul>
				</li>

				<!-- CRUD Tips -->
					<li class="<?php if ( isset($_GET['r']) && ($_GET['r']=='info' || $_GET['r']=='info/index' || $_GET['r']=='info/create' ) ) { echo 'active';}  ?>">
						<a href="#" class="dropdown-toggle">
							<i class="menu-icon fa fa-info"></i>
							Himbauan
							<b class="arrow fa fa-angle-down"></b>
						</a>
						<b class="arrow"></b>
						<ul class="submenu">
							<li class="<?php if (  isset($_GET['r']) && ($_GET['r']=='info' || $_GET['r']=='info/index') ) { echo 'active';}  ?>">
								<a href="<?php echo Yii::app()->createUrl('info/index'); ?>">
									Daftar Himbauan
								</a>
							</li>
							<li class="<?php if ( isset($_GET['r']) && ( $_GET['r']=='info/create' )) { echo 'active';}  ?>">
								<a href="<?php echo Yii::app()->createUrl('info/create'); ?>">
									Tambah Himbauan
								</a>
							</li>
						</ul>
					</li>

				<!-- Setting no list -->
				<li class="">
					<a href="<?php echo Yii::app()->createUrl('setting/index'); ?>">
						<i class="menu-icon fa fa-cogs"></i>
						<span class="menu-text"> Setting </span>
					</a>

					<b class="arrow"></b>
				</li>
			</ul><!-- /.nav-list -->

			<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
				<i class="ace-icon fa fa-angle-double-left" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
			</div>

			<script type="text/javascript">
				try{ace.settings.check('sidebar' , 'collapsed')}catch(e){}
			</script>
		</div>


		<div class="main-content" id="page">
			<div class="main-content-inner">
				<div class="breadcrumbs" id="breadcrumbs">
					<script type="text/javascript">
						try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
					</script>

					<ul class="breadcrumb">
						<?php if(isset($this->breadcrumbs)):?>
							<?php $this->widget('zii.widgets.CBreadcrumbs', array(
								'links'=>$this->breadcrumbs,
							)); ?><!-- breadcrumbs -->
						<?php endif?>
					</ul><!-- /.breadcrumb -->
				</div>
				<div class="page-content">
					<div class="row">
						<div class="col-xs-12">
							<div class="row">
								<div class="col-xs-12">
									<?php echo $content; ?>
								</div>
							</div>
						</div><!-- /.col -->
					</div><!-- /.row -->
				</div><!-- /.page-content -->
			</div>
		</div><!-- page -->

		<div class="footer">
				<div class="footer-inner">
					<div class="footer-content">
						<span class="bigger-120">
							<span class="blue bolder">Solusi</span>
							TM &copy; 2016
						</span>

						&nbsp; &nbsp;
						<span class="action-buttons">
							<a href="https://twitter.com/MalangGreenCity" target="_blank">
								<i class="ace-icon fa fa-twitter-square light-blue bigger-150"></i>
							</a>

							<a href="https://www.facebook.com/DKP-Kota-Malang-1423238194658334"  target="_blank">
								<i class="ace-icon fa fa-facebook-square text-primary bigger-150"></i>
							</a>

							<a href="http://dkp.malangkota.go.id/"  target="_blank">
								<i class="ace-icon fa fa-home orange bigger-150"></i>
							</a>
						</span>
					</div>
				</div>
		</div>
		<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
			<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
		</a>
	</div>

		
		<script src="js/jquery.2.1.1.min.js"></script>
		

		<script type="text/javascript">
			window.jQuery || document.write("<script src='js/jquery.min.js'>"+"<"+"/script>");
		</script>
		<script type="text/javascript">
			if('ontouchstart' in document.documentElement) document.write("<script src='js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
		</script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/jquery-ui.custom.min.js"></script>
		<script src="js/jquery.ui.touch-punch.min.js"></script>
		<script src="js/moment.min.js"></script>
		
		<!-- ace scripts -->
		<script src="js/ace-elements.min.js"></script>
		<script src="js/ace.min.js"></script>
	</body>
</html>
