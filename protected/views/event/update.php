<?php
/* @var $this EventController */
/* @var $model Event */

$this->breadcrumbs=array(
	'Kegiatan'=>array('index'),
	'Rubah',
);

$this->menu=array(
	array('label'=>'Create Event', 'url'=>array('create')),
	array('label'=>'List Event', 'url'=>array('index')),
);

$title = 'Rubah Kegiatan';
?>

<h1><?php echo $title?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>