<?php
/* @var $this EventController */
/* @var $model Event */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'event-form',
	'enableAjaxValidation'=>false,
		'htmlOptions' => array(
        'enctype' => 'multipart/form-data',
        'class'=>"form-horizontal"
    ),
)); ?>

	<p class="alert alert-block alert-success">Kolom dengan <span class="required">*</span> harus diisi.</p>

	<?php $errornya= $form->errorSummary($model); 
	if (strlen($errornya)>1) { ?>
		<div class="alert alert-danger">
			<button type="button" class="close" data-dismiss="alert">
				<i class="ace-icon fa fa-times"></i>
			</button>
			<strong>
				<i class="ace-icon fa fa-times"></i>
				Awas!
			</strong>
			<?php echo $form->errorSummary($model); ?>
			<br />
		</div>	
	<?php	} ?>
	
	<div class="form-group">
		<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> <?php echo $form->labelEx($model,'surat'); ?> </label>
		<div class="col-sm-9">
			<?php echo $form->textField($model,'surat',array('size'=>60,'maxlength'=>128,  'id'=>"surat", 'placeholder'=>"Nomor Surat", 'class'=>"col-xs-12")); ?>
			<?php echo $form->error($model,'surat'); ?>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> <?php echo $form->labelEx($model,'tgl_surat'); ?> </label>
		<div class="col-sm-3">
			<div class="input-group">
				<?php echo $form->textField($model,'tgl_surat', array('id' => 'id-date-picker-1', 'class' => 'form-control date-picker',  'data-date-format'=>"yyyy-mm-dd" )); ?>
				<span class="input-group-addon">
					<i class="fa fa-calendar-o bigger-110"></i>
				</span>
			</div>
			<?php echo $form->error($model,'tgl_surat'); ?>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> <?php echo $form->labelEx($model,'schedule'); ?> </label>
		<div class="col-sm-3">
			<div class="input-group">
				<?php echo $form->textField($model,'schedule', array('id' => 'date-timepicker1', 'class' => 'form-control')); ?>
				<span class="input-group-addon">
					<i class="fa fa-clock-o bigger-110"></i>
				</span>
			</div>
			<?php echo $form->error($model,'schedule'); ?>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> <?php echo $form->labelEx($model,'name'); ?> </label>
		<div class="col-sm-9">
			<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>250,  'id'=>"form-field-1", 'placeholder'=>"Acara", 'class'=>"col-xs-12")); ?>
			<?php echo $form->error($model,'name'); ?>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> <?php echo $form->labelEx($model,'place'); ?> </label>
		<div class="col-xs-12 col-sm-9">
			<?php $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
			    'model'=>$model,
			    'attribute'=>'place',
			    'sourceUrl'=>Yii::app()->createUrl('event/fetchPlace'),
				    'options'=>array(
				        'minLength'=>'2',
				    ),
				    'htmlOptions'=>array('size'=>40),
	    		)); 
	    	?>
			<?php echo $form->error($model,'place'); ?>
		</div>
	</div> 

	<div class="form-group">
		<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> <?php echo $form->labelEx($model,'disposisi_nama'); ?> </label>
		<div class="col-sm-9">		
			<?php echo $form->textArea($model,'disposisi_nama',array('size'=>60,'maxlength'=>250,  'id'=>"form-field-11", 'placeholder'=>"Disposisi Nama", 'class'=>"col-xs-12")); ?>
			<?php echo $form->error($model,'disposisi_nama'); ?>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> <?php echo $form->labelEx($model,'disposisi_jabatan'); ?> </label>
		<div class="col-sm-9">		
			<?php echo $form->textArea($model,'disposisi_jabatan',array('size'=>60,'maxlength'=>250,  'id'=>"form-field-11", 'placeholder'=>"Disposisi Nama", 'class'=>"col-xs-12")); ?>
			<?php echo $form->error($model,'disposisi_jabatan'); ?>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> <?php echo $form->labelEx($model,'remark'); ?> </label>
		<div class="col-sm-9">
			<?php echo $form->textArea($model,'remark',array('class'=> 'form-control', 'rows'=>'6','maxlength'=>250 )); ?>
		</div>
	</div>

	<div class="clearfix form-actions">
		<div class="col-md-offset-3 col-md-9">
			<?php echo CHtml::submitButton($model->isNewRecord ? ' Tambahkan' : '  Simpan', 
			array('class' => 'btn btn-primary', 'type' => 'submit')); ?>
			
			
		</div>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->

<?php
$scripts=<<<EOL
				$('.date-picker').datepicker({
					autoclose: true,
					todayHighlight: true,
					dateFormat: 'yy-mm-dd' 
				})
				//show datepicker when clicking on the icon
				.next().on(ace.click_event, function(){
					$(this).prev().focus();
				});
					
				//Android's default browser somehow is confused when tapping on label which will lead to dragging the task
				//so disable dragging when clicking on label
				var agent = navigator.userAgent.toLowerCase();
				if("ontouchstart" in document && /applewebkit/.test(agent) && /android/.test(agent))
				  $('#tasks').on('touchstart', function(e){
					var li = $(e.target).closest('#tasks li');
					if(li.length == 0)return;
					var label = li.find('label.inline').get(0);
					if(label == e.target || $.contains(label, e.target)) e.stopImmediatePropagation() ;
				});
$('#date-timepicker1').datetimepicker({
	format: 'YYYY-MM-DD HH:mm',
	sideBySide: true,
	pickerPosition: "bottom-left"
}).next().on(ace.click_event, function(){
	$(this).prev().focus();
});
$('.chosen-select').chosen({allow_single_deselect:true});
EOL;

/*******
 * JavaScript
 *******/
Yii::app()->clientScript->registerScript('create-event', $scripts, CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl ."/backend/js/bootstrap-datetimepicker.min.js", CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl ."/backend/js/bootstrap-datepicker.min.js", CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl ."/backend/js/chosen.jquery.min.js", CClientScript::POS_END);

/*******
 * Css
 *******/
Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl ."/backend/css/datepicker.min.css");
Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl ."/backend/css/bootstrap-datetimepicker.min.css");
Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl ."/backend/css/chosen.min.css");

