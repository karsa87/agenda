<?php
/* @var $this EventController */
/* @var $model Event */

$this->breadcrumbs=array(
	'Kegiatan'=>array('index'),
	'Jadwal',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-form form').submit(function(){
	$('#event-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
$title = "Daftar Kegiatan";
?>

<h1><?php echo $title;?></h1>
<div class="clearfix">
	<div class="pull-right tableTools-container">
		<div class="btn-group btn-overlap">
		<?php 
			if(array_key_exists('Event', $_GET)) { 
				$saring='?r=event/export';
				foreach ($_GET['Event'] as $key =>$value) {
					$saring=$saring.'&'.$key.'='.$value;
				} ?>
				<a href="../index.php<?php echo $saring; ?>"  class="DTTT_button btn btn-white btn-primary  btn-bold" id="ToolTables_dynamic-table_1" tabindex="0" aria-controls="dynamic-table" > Excel</a>
			<?php 
			} else { 
				?>
				<a onclick="export_function();" class="btn btn-app btn-grey btn-xs radius-4" id="ToolTables_dynamic-table_1" tabindex="0" aria-controls="dynamic-table"> 
					<span><i class="ace-icon fa fa-file-excel-o bigger-160"></i></span>
					<div style="position: absolute; left: 0px; top: 0px; width: 39px; height: 35px; z-index: 99;" title="" data-original-title="Export to CSV">
					</div>
				</a> <?php
		} ?>
		</div>
	</div>
</div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'event-grid',
	'dataProvider'=>$model->search(),
	'itemsCssClass'=>'table table-striped table-bordered table-hover',
	'enableSorting'=>true,
	'filter'=>$model,
	'columns'=>array(
		'surat',
		'name',
		array(
			'header' => 'Tgl. Acara',
            'name' => 'schedule',
            'filter' => $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model'=>$model, 
                'attribute'=>'schedule', 
                'language' => 'id',
                'htmlOptions' => array(
                    'id' => 'datepicker_for_due_date',
                    'size' => '10',
                ),
                'defaultOptions' => array(  // (#3)
                    'showOn' => 'focus', 
                    'dateFormat' => 'yy/mm/dd',
                    'showOtherMonths' => true,
                    'selectOtherMonths' => true,
                    'changeMonth' => true,
                    'changeYear' => true,
                    'showButtonPanel' => true,
                )
            ), 
            true), 
            'value' => 'date("D / d M Y", strtotime($data->schedule))',
        ),
		array(
			'name'=> 'schedule',
			'header' => 'Waktu',
			'value' => 'date("H:i", strtotime($data->schedule))',
		),
		'place',
		'disposisi_nama',
		'disposisi_jabatan',
		array(
	        'name'=>'is_active',             
	        'value'=>'',
	        'type'=>'raw',
	        'htmlOptions'=>array('width'=>5),
	        'value'=>'sprintf(\'<input  onchange="toggleAktif(this.value);" type="checkbox" class="ace ace-switch ace-switch-5 toggle-status" %s  value="%s"  /><span class="lbl"></span>\', ($data->is_active == 1 ? \'checked="checked"\' : ""), $data->id)',
	        'filter' => array('1'=>'Ya','0'=>'Tidak'),
        ),
		array(
				'class'=>'CButtonColumn',
									'template'=>'{update} {delete}',
									'htmlOptions'=>array('class'=>'col-xs-2 col-sm-2 col-md-1'),
								    'buttons'=>array (
								        'update'=> array(
								            'label'=>'<i class="fa fa-pencil"></i>',
								            'imageUrl'=>false,
								            'options'=>array( 'title'=> 'Ubah', 'class'=>'btn btn-warning btn-xs' ),
								        ),								        
								        'delete'=>array(
								            'label'=>'<i class="fa fa-remove"></i>',
								            'imageUrl'=>false,
								            'options'=>array( 'title'=> 'Hapus', 'class'=>'btn btn-danger btn-xs' ),
								        ),
								    ),
			),
	),
)); ?>


<?php
$url = Yii::app()->createUrl('event/export');
$url_toggle = Yii::app()->createUrl('event/toggle');

$scripts=<<<EOL
var export_function = function(){
	var no_surat = 'surat=' + $( "input[name='Event[surat]']" ).val();
	var acara = 'name=' + $( "input[name='Event[name]']" ).val();
	var tgl_acara = 'schedule=' + $( "input[name='Event[schedule]']" ).val();
	var tempat = 'place=' + $( "input[name='Event[place]']" ).val();
	var disposisi_nama = 'disposisi_nama=' + $( "input[name='Event[disposisi_nama]']" ).val();
	var disposisi_jabatan = 'disposisi_jabatan=' + $( "input[name='Event[disposisi_jabatan]']" ).val();

	window.location='$url'+'&'+no_surat+'&'+acara+'&'+tgl_acara+'&'+tempat+'&'+disposisi_nama+'&'+disposisi_jabatan;
}

var toggleAktif = function(idnya){
	console.log('masuk');
	var url = '$url_toggle'+'&id='+idnya;
    window.location.assign(url);
}
EOL;

/*******
 * JavaScript
 *******/
Yii::app()->clientScript->registerScript('create-event', $scripts, CClientScript::POS_END);