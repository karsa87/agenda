<?php
/* @var $this EventController */
/* @var $model Event */

$this->breadcrumbs=array(
	'Kegiatan'=>array('index'),
	'Tambah',
);

$this->menu=array(
	array('label'=>'List Event', 'url'=>array('index')),
);
$title = "Tambah Kegiatan";
?>

<h1><?php echo $title;?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>