<input type="hidden" id="next-page" value="<?php echo $page; ?>" />
<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'event-grid',
    'dataProvider'=>$provider,
    'columns'=>array(
        'name',
        'schedule',
        'place',
        'remark',
    ),
));