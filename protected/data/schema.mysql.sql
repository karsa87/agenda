CREATE TABLE tbl_user (
    id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    username VARCHAR(128) NOT NULL,
    password VARCHAR(128) NOT NULL,
    full_name VARCHAR(128) NOT NULL,
    is_active TINYINT(1) DEFAULT 1,
    position varchar(150),
    last_login timestamp
) ENGINE=InnoDB ;

CREATE TABLE tbl_event (
    id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(250) NOT NULL,
    surat varchar(128) DEFAULT NULL,
    tgl_surat date DEFAULT NULL,
    schedule DATETIME,
    place VARCHAR(150),
    remark VARCHAR(250),
    disposisi_nama varchar(128) DEFAULT NULL,
    disposisi_jabatan varchar(128) DEFAULT NULL,
    is_active TINYINT(1) DEFAULT 1,
    create_by integer,
    create_at timestamp,
    updated_by integer,
    updated_at timestamp
) ENGINE=InnoDB ;

CREATE TABLE tbl_info (
    id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    category varchar(100),
    title VARCHAR(150),
    content TEXT,
    image_path varchar(250),
    is_active TINYINT(1) DEFAULT 1
) ENGINE=InnoDB ;

CREATE TABLE IF NOT EXISTS `tbl_setting` (
  `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `sidebar` varchar(256) DEFAULT NULL,
  `date_from` INT DEFAULT NULL,
  `date_to` INT DEFAULT NULL,
  `text_footer` varchar(250) NOT NULL,
  `limit_row` int(5) DEFAULT NULL,
  `limit_column` text,
  `interval_time` int(5) DEFAULT NULL,
  `facebook` varchar(225) NOT NULL,
  `url_facebook` text NOT NULL,
  `twitter` varchar(225) NOT NULL,
  `url_twitter` text NOT NULL,
  `email1` varchar(225) NOT NULL,
  `email2` varchar(225) NOT NULL,
  `website` varchar(225) NOT NULL,
  `text_header1` varchar(225) NOT NULL,
  `text_header2` varchar(225) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=UTF8;

CREATE VIEW `grafik_per_bulan` AS 
  SELECT `tbl_event`.`name` AS `name`,count(`tbl_event`.`name`) AS `kali`, month(`tbl_event`.`schedule`) AS `bulan`
  FROM `tbl_event` 
  GROUP BY month(`tbl_event`.`schedule`);

CREATE VIEW `tempat_terbanyak` AS 
  SELECT count(`tbl_event`.`place`) AS `jml_tempat`,`tbl_event`.`place` AS `place` 
  FROM `tbl_event`
  GROUP BY `tbl_event`.`place` limit 5;
